package br.com.viattec.imobapp.http;

import android.util.Log;

import com.parse.ParseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.com.viattec.imobapp.model.Quadra;

/**
 * Created by Jaciel on 14/09/2015.
 */
public class PlanilhaHttp {

    public static void quadraList() throws JSONException {
        try {
            URL url = new URL("https://www.dropbox.com/s/n92vsv0jrxiudfz/json.txt?dl=1");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(10000);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");
            connection.connect();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                parseJSONPlanilha(connection.getInputStream());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void parseJSONPlanilha(InputStream inputStream) throws JSONException, IOException {
        /*List<Quadra> quadras = new ArrayList<>();
        JSONObject table = new JSONObject(streamToString(inputStream));
        JSONArray rows = table.getJSONObject("table").getJSONArray("rows");
        String quadra = "";
        for (int i = 0; i < rows.length(); i++) {
            JSONArray c = rows.getJSONObject(i).getJSONArray("c");

            if (i >= 3) {
                try {
                    if (c.getJSONObject(0) != null) {
                        quadra = c.getJSONObject(0).getString("v");
                    }
                }catch (Exception e){
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }
            }
            if (i >= 3) {
                try {
                    ParseObject lote = new ParseObject("Lote");
                    lote.put("quadra",quadra);
                    lote.put("numLote", c.getJSONObject(1).getString("v"));
                    lote.put("areaLote", c.getJSONObject(2).getString("v"));
                    lote.put("valorLote", c.getJSONObject(3).getString("v"));
                    lote.put("valorAVista", c.getJSONObject(4).getString("v"));
                    lote.put("valorEntrada", c.getJSONObject(5).getString("v"));
                    lote.put("valorParcela60", c.getJSONObject(6).getString("v"));
                    lote.put("valorParcela100", c.getJSONObject(7).getString("v"));
                   // lote.saveInBackground();
                }catch (Exception e){
                    Log.e("JSON Parser", "Linha nula " + e.toString());
                }
            }
        }*/
    }

    private static String streamToString(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int qtdeBytesLidos;

        while ((qtdeBytesLidos = inputStream.read(buffer)) > 0 ){
            baos.write(buffer, 0, qtdeBytesLidos);
        }
        return new String(baos.toByteArray());
    }
}
