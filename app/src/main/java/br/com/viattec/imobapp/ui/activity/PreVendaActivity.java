package br.com.viattec.imobapp.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

import br.com.viattec.imobapp.R;
import br.com.viattec.imobapp.model.Cliente;

public class PreVendaActivity extends AppCompatActivity {

    ArrayAdapter<Cliente> mAdapter;
    List<Cliente> mClientes;
    Spinner spClientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_venda);
        spClientes = (Spinner) findViewById(R.id.spClientes);
        carregaClientes();
        spClientes.setAdapter(mAdapter);
    }

    public void carregaClientes(){
        ParseQuery<Cliente> query = ParseQuery.getQuery(Cliente.class);
        query.fromLocalDatastore();
        query.orderByAscending("nome");
        query.findInBackground(new FindCallback<Cliente>() {
            @Override
            public void done(List<Cliente> list, ParseException e) {
                if (e == null) {
                    if(mClientes != null) {
                        mClientes.clear();
                    }
                    for (ParseObject cliente : list) {
                        String id = cliente.getObjectId();
                        String nome = cliente.getString("nome");
                        String cpf = cliente.getString("cpf");
                        String rg = cliente.getString("rg");
                        String logradouro = cliente.getString("logradouro");
                        String numero = cliente.getString("numero");
                        String bairro = cliente.getString("bairro");
                        String cep = cliente.getString("cep");
                        String cidade = cliente.getString("cidade");
                        String uf = cliente.getString("uf");
                        String telefone1 = cliente.getString("telefone1");
                        String telefone2 = cliente.getString("telefone2");
                        String telefone3 = cliente.getString("telefone3");
                        boolean wa1 = cliente.getBoolean("wa1");
                        boolean wa2 = cliente.getBoolean("wa2");
                        boolean wa3 = cliente.getBoolean("wa3");
                        String estadoCivil = cliente.getString("estadoCivil");
                        Cliente novoCliente = new Cliente(id, nome, cpf, rg, logradouro, numero, bairro, cep, cidade, uf, telefone1, telefone2, telefone3, wa1, wa2,
                                wa3,estadoCivil);
                        mClientes.add(novoCliente);
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pre_venda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
