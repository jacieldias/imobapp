package br.com.viattec.imobapp.application;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;

import br.com.viattec.imobapp.model.Cliente;
import br.com.viattec.imobapp.model.Loteamento;
import br.com.viattec.imobapp.model.Lotes;

/**
 * Created by Jaciel on 02/11/2015.
 */
public class ImobAppApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(Cliente.class);
        ParseObject.registerSubclass(Lotes.class);
        ParseObject.registerSubclass(Loteamento.class);
        Parse.initialize(this, "GUuJddBqh9RHAm1pPewhTpDaRwjUv1NPfcniChmT", "6SylR9BJv8XIqmpV9Koon6KbxB4glCEsnzvqAtJY");
    }
}

