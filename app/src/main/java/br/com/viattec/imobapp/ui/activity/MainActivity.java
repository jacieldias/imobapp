package br.com.viattec.imobapp.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.parse.Parse;
import com.parse.ParseObject;

import org.json.JSONException;

import java.util.List;

import br.com.viattec.imobapp.R;
import br.com.viattec.imobapp.http.PlanilhaHttp;
import br.com.viattec.imobapp.model.Cliente;
import br.com.viattec.imobapp.model.PreVenda;
import br.com.viattec.imobapp.model.Quadra;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    PlanilhaAsyncTask mTask;
    List<Quadra> mQuadras;
    Button btnCliente;
    Button btnLoteamento;
    Button btnAtualizar;
    Button btnPreVenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCliente = (Button) findViewById(R.id.btnCliente);
        btnLoteamento = (Button) findViewById(R.id.btnLoteamento);
        btnAtualizar = (Button) findViewById(R.id.btnAtualizar);
        btnPreVenda = (Button) findViewById(R.id.btnPreVenda);
        btnCliente.setOnClickListener(this);
        btnAtualizar.setOnClickListener(this);
        btnPreVenda.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnAtualizar) {
            mTask = new PlanilhaAsyncTask();
            mTask.execute();
        }
        if (v.getId() == R.id.btnCliente) {
            Intent intent = new Intent(MainActivity.this,ClienteActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.btnPreVenda){
            Intent intent = new Intent(MainActivity.this, PreVendaActivity.class);
            startActivity(intent);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class PlanilhaAsyncTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... params) {
            try {
                PlanilhaHttp.quadraList();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
