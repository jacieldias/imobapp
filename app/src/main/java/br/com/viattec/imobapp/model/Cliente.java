package br.com.viattec.imobapp.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Jaciel on 13/09/2015.
 */
@ParseClassName("Cliente")
public class Cliente extends ParseObject{

    private String id;
    private String nome;
    private String cpf;
    private String rg;
    private String logradouro;
    private String numero;
    private String bairro;
    private String cep;
    private String cidade;
    private String uf;
    private String telefone1;
    private String telefone2;
    private String telefone3;
    private boolean wa1;
    private boolean wa2;

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    private boolean wa3;
    private String estadoCivil;

    public Cliente(){

    }


    public Cliente(String id, String nome, String cpf, String rg, String logradouro, String numero, String bairro, String cep, String cidade, String uf, String telefone1, String telefone2, String telefone3, boolean wa1, boolean wa2, boolean wa3, String estadoCivil) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.logradouro = logradouro;
        this.numero = numero;
        this.bairro = bairro;
        this.cep = cep;
        this.cidade = cidade;
        this.uf = uf;
        this.telefone1 = telefone1;
        this.telefone2 = telefone2;
        this.telefone3 = telefone3;
        this.wa1 = wa1;
        this.wa2 = wa2;
        this.wa3 = wa3;
        this.estadoCivil = estadoCivil;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public String getTelefone3() {
        return telefone3;
    }

    public void setTelefone3(String telefone3) {
        this.telefone3 = telefone3;
    }

    public boolean isWa1() {
        return wa1;
    }

    public void setWa1(boolean wa1) {
        this.wa1 = wa1;
    }

    public boolean isWa2() {
        return wa2;
    }

    public void setWa2(boolean wa2) {
        this.wa2 = wa2;
    }

    public boolean isWa3() {
        return wa3;
    }

    public void setWa3(boolean wa3) {
        this.wa3 = wa3;
    }
}
