package br.com.viattec.imobapp.ui.activity;

import android.content.Intent;
import android.net.ParseException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import br.com.viattec.imobapp.R;
import br.com.viattec.imobapp.model.Cliente;

public class ClienteActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnSalvar;
    Button btnListar;
    EditText edtNome;
    EditText edtCpf;
    EditText edtRg;
    EditText edtLogradouro;
    EditText edtNumero;
    EditText edtBairro;
    EditText edtCidade;
    EditText edtCep;
    EditText edtUF;
    EditText edtTelefone1;
    EditText edtTelefone2;
    EditText edtTelefone3;
    CheckBox wa1;
    CheckBox wa2;
    CheckBox wa3;
    RadioButton rdCasado;
    RadioButton rdSolteiro;
    RadioButton rdOutros;

    boolean alterarCliente = false;
    String idCliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);
        btnSalvar = (Button)findViewById(R.id.btnSalvar);
        btnListar = (Button)findViewById(R.id.btnListar);
        edtNome = (EditText) findViewById(R.id.edtNome);
        edtTelefone1 = (EditText) findViewById(R.id.edtTelefone1);
        edtTelefone2 = (EditText) findViewById(R.id.edtTelefone2);
        edtTelefone3 = (EditText) findViewById(R.id.edtTelefone3);
        edtCpf = (EditText) findViewById(R.id.edtCPF);
        edtRg = (EditText) findViewById(R.id.edtRG);
        edtLogradouro = (EditText) findViewById(R.id.edtLogradouro);
        edtNumero = (EditText) findViewById(R.id.edtNumero);
        edtBairro = (EditText) findViewById(R.id.edtBairro);
        edtCep = (EditText) findViewById(R.id.edtCep);
        edtCidade = (EditText) findViewById(R.id.edtCidade);
        edtUF = (EditText) findViewById(R.id.edtEstado);
        wa1 = (CheckBox) findViewById(R.id.chkWa1);
        wa2 = (CheckBox) findViewById(R.id.chkWa2);
        wa3 = (CheckBox) findViewById(R.id.chkWa3);
        rdCasado = (RadioButton) findViewById(R.id.rdCasado);
        rdSolteiro = (RadioButton) findViewById(R.id.rdSolteiro);
        rdOutros = (RadioButton) findViewById(R.id.rdOutros);
        btnSalvar.setOnClickListener(this);
        btnListar.setOnClickListener(this);

        Intent intent = getIntent();
        alterarCliente = intent.getBooleanExtra("alterarCliente", false);
        if(alterarCliente){
            idCliente = intent.getStringExtra("idCliente");
            buscaClientePorId(idCliente);
        }
    }
    public void buscaClientePorId(String id){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Cliente");
        query.fromLocalDatastore();
        query.getInBackground(id, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                if (e == null) {
                    Cliente cliente = (Cliente) parseObject;
                    preencheView(cliente);
                } else {
                    Toast.makeText(ClienteActivity.this, "Cliente não encontrado!",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void preencheView(Cliente cliente){
        edtNome.setText(cliente.getString("nome"));
        edtCpf.setText(cliente.getString("cpf"));
        edtRg.setText(cliente.getString("rg"));
        edtLogradouro.setText(cliente.getString("logradouro"));
        edtNumero.setText(cliente.getString("numero"));
        edtBairro.setText(cliente.getString("bairro"));
        edtCidade.setText(cliente.getString("cidade"));
        edtCep.setText(cliente.getString("cep"));
        edtUF.setText(cliente.getString("uf"));
        edtTelefone1.setText(cliente.getString("telefone1"));
        edtTelefone2.setText(cliente.getString("telefone2"));
        edtTelefone3.setText(cliente.getString("telefone3"));
        wa1.setChecked(cliente.getBoolean("wa1"));
        wa2.setChecked(cliente.getBoolean("wa2"));
        wa3.setChecked(cliente.getBoolean("wa3"));
        String estadoCivil = cliente.getString("estadoCivil");
        if(estadoCivil.equals("C")){
            rdCasado.setChecked(true);
        }
        if(estadoCivil.equals("S")){
            rdSolteiro.setChecked(true);
        }
        if(estadoCivil.equals("O")){
            rdOutros.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnSalvar) {
            if (edtNome.getText().length() <= 0) {
                Toast.makeText(this,"Informe o nome do cliente!",Toast.LENGTH_LONG).show();
                return;
            }
            salvarCliente();
        }
        if (v.getId() == R.id.btnListar) {
            Intent intent = new Intent(this,ListaClienteActivity.class);
            startActivity(intent);
        }
    }

    public void atualizarCliente(String id){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Cliente");
        query.fromLocalDatastore();
        query.getInBackground(id, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                if (e == null) {
                    Cliente cliente = (Cliente) parseObject;
                    //Bind dos componentes
                    String nome = edtNome.getText().toString().trim();
                    String cpf = edtCpf.getText().toString().trim();
                    String rg = edtRg.getText().toString().trim();
                    String logradouro = edtLogradouro.getText().toString().trim();
                    String numero = edtNumero.getText().toString().trim();
                    String bairro = edtBairro.getText().toString().trim();
                    String cep = edtCep.getText().toString().trim();
                    String cidade = edtCidade.getText().toString().trim();
                    String uf = edtUF.getText().toString().trim();
                    String telefone1 = edtTelefone1.getText().toString().trim();
                    String telefone2 = edtTelefone2.getText().toString().trim();
                    String telefone3 = edtTelefone3.getText().toString().trim();
                    boolean w1 = wa1.isChecked();
                    boolean w2 = wa2.isChecked();
                    boolean w3 = wa3.isChecked();
                    String estadoCivil = "";
                    if(rdCasado.isChecked()){
                        estadoCivil = "C";
                    }
                    if(rdSolteiro.isChecked()){
                        estadoCivil = "S";
                    }
                    if(rdOutros.isChecked()){
                        estadoCivil = "O";
                    }

                    cliente.put("nome",nome);
                    cliente.put("cpf",cpf);
                    cliente.put("rg",rg);
                    cliente.put("logradouro",logradouro);
                    cliente.put("numero",numero);
                    cliente.put("bairro",bairro);
                    cliente.put("cep",cep);
                    cliente.put("cidade",cidade);
                    cliente.put("uf",uf);
                    cliente.put("telefone1",telefone1);
                    cliente.put("telefone2",telefone2);
                    cliente.put("telefone3",telefone3);
                    cliente.put("wa1",w1);
                    cliente.put("wa2",w2);
                    cliente.put("wa3",w3);
                    cliente.put("estadoCivil",estadoCivil);
                    //Atualizando localmente
                    cliente.pinInBackground();
                    //Atualizando no Parse
                    cliente.saveEventually();
                    Toast.makeText(ClienteActivity.this, "Cliente atualizado!",Toast.LENGTH_LONG).show();
                    novoCliente();
                    finish();
                    alterarCliente = false;
                } else {
                    Toast.makeText(ClienteActivity.this, "Cliente não pode ser atualizado!",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public void deleteCliente(String id){
        if(alterarCliente){
            ParseQuery<ParseObject> query = ParseQuery.getQuery("Cliente");
            query.fromLocalDatastore();
            query.getInBackground(id, new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, com.parse.ParseException e) {
                    if (e == null) {
                        Cliente cliente = (Cliente) parseObject;
                        parseObject.deleteEventually();
                        parseObject.unpinInBackground();
                        Toast.makeText(ClienteActivity.this, "Cliente Excluído!", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ClienteActivity.this, "Cliente não encontrado!", Toast.LENGTH_LONG).show();
                    }
                }
            });
            novoCliente();
            finish();
        }else{
            Toast.makeText(this,"Não foi possível excluir este cliente!",Toast.LENGTH_LONG).show();
        }
    }

    public void salvarCliente(){
        //Bind das informações
        String nome = edtNome.getText().toString().trim();
        String cpf = edtCpf.getText().toString().trim();
        String rg = edtRg.getText().toString().trim();
        String logradouro = edtLogradouro.getText().toString().trim();
        String numero = edtNumero.getText().toString().trim();
        String bairro = edtBairro.getText().toString().trim();
        String cep = edtCep.getText().toString().trim();
        String cidade = edtCidade.getText().toString().trim();
        String uf = edtUF.getText().toString().trim();
        String telefone1 = edtTelefone1.getText().toString().trim();
        String telefone2 = edtTelefone2.getText().toString().trim();
        String telefone3 = edtTelefone3.getText().toString().trim();
        boolean w1 = wa1.isChecked();
        boolean w2 = wa2.isChecked();
        boolean w3 = wa3.isChecked();
        String estadoCivil = "";
        if(rdCasado.isChecked()){
            estadoCivil = "C";
        }
        if(rdSolteiro.isChecked()){
            estadoCivil = "S";
        }
        if(rdOutros.isChecked()){
            estadoCivil = "O";
        }

        ParseObject cliente = ParseObject.create("Cliente");
        cliente.put("nome",nome);
        cliente.put("cpf",cpf);
        cliente.put("rg",rg);
        cliente.put("logradouro",logradouro);
        cliente.put("numero",numero);
        cliente.put("bairro",bairro);
        cliente.put("cep",cep);
        cliente.put("cidade",cidade);
        cliente.put("uf",uf);
        cliente.put("telefone1",telefone1);
        cliente.put("telefone2",telefone2);
        cliente.put("telefone3",telefone3);
        cliente.put("wa1",w1);
        cliente.put("wa2",w2);
        cliente.put("wa3",w3);
        cliente.put("estadoCivil",estadoCivil);
        //Salvando localmente
        cliente.pinInBackground();
        //Salvando no Parse
        cliente.saveEventually();

        Toast.makeText(getApplicationContext(),"Cliente salvo!",Toast.LENGTH_SHORT).show();
        novoCliente();
    }

    public void novoCliente(){
        edtNome.setText("");
        edtCpf.setText("");
        edtRg.setText("");
        edtLogradouro.setText("");
        edtNumero.setText("");
        edtBairro.setText("");
        edtCep.setText("");
        edtCidade.setText("");
        edtUF.setText("");
        edtTelefone1.setText("");
        edtTelefone2.setText("");
        edtTelefone3.setText("");
        wa1.setChecked(false);
        wa2.setChecked(false);
        wa3.setChecked(false);
        rdCasado.setChecked(false);
        rdSolteiro.setChecked(false);
        rdOutros.setChecked(false);
        edtNome.requestFocus();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cliente, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.action_save){
            if (edtNome.getText().length() <= 0) {
                Toast.makeText(this,"Informe o nome do cliente!",Toast.LENGTH_LONG).show();
                return false;
            }
            if(!alterarCliente) {
                salvarCliente();
            }else{
                atualizarCliente(idCliente);
            }

        }

        if(id == R.id.action_delete){
            deleteCliente(idCliente);
        }

        if(id == R.id.action_list){
            Intent intent = new Intent(this,ListaClienteActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        alterarCliente = false;
    }
}
