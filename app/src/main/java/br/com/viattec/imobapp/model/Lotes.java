package br.com.viattec.imobapp.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.math.BigDecimal;

/**
 * Created by Jaciel on 14/09/2015.
 */
@ParseClassName("Lotes")
public class Lotes extends ParseObject{
    private String id;
    private Quadra quadra;
    private int numeroLote;
    private Double areaLote;
    private Double valorLote;
    private Double valorLoteAVista;
    private Double valorEntrada;
    private Double valorParcela60;
    private Double valorParcela100;
    private int nivelDeAcidente;
    private boolean deEsquina;
    private Double frente;
    private Double fundo;
    private Double ladoEsquerdo;
    private Double ladoDireito;
}
