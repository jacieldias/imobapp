package br.com.viattec.imobapp.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Jaciel on 15/09/2015.
 */
public class ImobAppSQLHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "imobdb";

    public ImobAppSQLHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table quadras (_id integer primary key autoincrement,"
                + "descricao text not null, fabricante integer, placa text, ano integer)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
