package br.com.viattec.imobapp.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

import br.com.viattec.imobapp.R;
import br.com.viattec.imobapp.model.Cliente;
import br.com.viattec.imobapp.ui.adapter.ClienteAdapter;
import br.com.viattec.imobapp.util.ImobAppUtil;

public class ListaClienteActivity extends AppCompatActivity implements ClienteAdapter.AoClicarNoClienteListener, View.OnClickListener {

    RecyclerView mRecyclerView;
    ClienteAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    final List<Cliente> mClientes = new ArrayList<Cliente>();
    Button btnBuscar;
    EditText edtBusca;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cliente);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        edtBusca = (EditText) findViewById(R.id.edtBusca);
        btnBuscar.setOnClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        listarClientes();
        Log.d("mClientes",String.valueOf(mClientes.size()));
        mAdapter = new ClienteAdapter(this, mClientes);
        mAdapter.setAoClicarNoClienteListener(this);

        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_lista_cliente, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.action_new){
            Intent intent = new Intent(ListaClienteActivity.this, ClienteActivity.class);
            startActivity(intent);
        }

        if(id == R.id.action_sync){
            if(ImobAppUtil.isCheckNetwork(ListaClienteActivity.this)){
                sincronizarClientes();
            }else{
                Toast.makeText(this,"Você não possui uma conexão de internet!",Toast.LENGTH_LONG).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void aoClicarNoCliente(View v, int position, Cliente cliente, String idCliente) {
        Intent itAlteraCliente = new Intent(ListaClienteActivity.this,ClienteActivity.class);
        itAlteraCliente.putExtra("idCliente", idCliente);
        itAlteraCliente.putExtra("alterarCliente", true);
        startActivity(itAlteraCliente);
    }

    public void listarClientes() {
        final ProgressDialog dialog = new ProgressDialog(ListaClienteActivity.this);
        dialog.setMessage(getString(R.string.message_dialog_lista_cliente));
        //dialog.setCancelable(false);
        dialog.show();

        ParseQuery<Cliente> query = ParseQuery.getQuery(Cliente.class);
        query.fromLocalDatastore();
        query.orderByAscending("nome");
        query.findInBackground(new FindCallback<Cliente>() {
            @Override
            public void done(List<Cliente> list, ParseException e) {
                if (e == null) {
                    dialog.dismiss();
                    carregarListaCliente(list);
                }
            }
        });
    }

    public void sincronizarClientes() {
        final ProgressDialog dialog = new ProgressDialog(ListaClienteActivity.this);
        dialog.setMessage(getString(R.string.message_dialog_lista_cliente));
        //dialog.setCancelable(false);
        dialog.show();

        ParseObject.unpinAllInBackground("Cliente");

        ParseQuery<Cliente> querySync = ParseQuery.getQuery(Cliente.class);
        querySync.orderByAscending("nome");
        querySync.findInBackground(new FindCallback<Cliente>() {
            @Override
            public void done(List<Cliente> list, ParseException e) {
                if (e == null) {
                    ParseObject.pinAllInBackground("Cliente",list);
                    dialog.dismiss();
                    listarClientes();
                }
            }
        });
    }


    public void carregarListaCliente(List<Cliente> list){
        mClientes.clear();
        for (ParseObject cliente : list) {
            String id = cliente.getObjectId();
            String nome = cliente.getString("nome");
            String cpf = cliente.getString("cpf");
            String rg = cliente.getString("rg");
            String logradouro = cliente.getString("logradouro");
            String numero = cliente.getString("numero");
            String bairro = cliente.getString("bairro");
            String cep = cliente.getString("cep");
            String cidade = cliente.getString("cidade");
            String uf = cliente.getString("uf");
            String telefone1 = cliente.getString("telefone1");
            String telefone2 = cliente.getString("telefone2");
            String telefone3 = cliente.getString("telefone3");
            boolean wa1 = cliente.getBoolean("wa1");
            boolean wa2 = cliente.getBoolean("wa2");
            boolean wa3 = cliente.getBoolean("wa3");
            String estadoCivil = cliente.getString("estadoCivil");
            Cliente novoCliente = new Cliente(id, nome, cpf, rg, logradouro, numero, bairro, cep, cidade, uf, telefone1, telefone2, telefone3, wa1, wa2,
                    wa3,estadoCivil);
            mClientes.add(novoCliente);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mClientes.clear();
        listarClientes();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnBuscar){
            ParseQuery<Cliente> query = ParseQuery.getQuery(Cliente.class);
            query.fromLocalDatastore();
            query.whereMatches("nome", "(" + edtBusca.getText().toString()+")", "i");
            query.findInBackground(new FindCallback<Cliente>() {
                @Override
                public void done(List<Cliente> list, ParseException e) {
                    if(e == null){
                        mClientes.clear();
                        carregarListaCliente(list);
                        edtBusca.setText("");
                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
