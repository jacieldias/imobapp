package br.com.viattec.imobapp.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.util.List;

/**
 * Created by Jaciel on 14/09/2015.
 */

@ParseClassName("Loteamento")
public class Loteamento extends ParseObject{

    private String id;
    private String nome;
    private String cidade;
}
