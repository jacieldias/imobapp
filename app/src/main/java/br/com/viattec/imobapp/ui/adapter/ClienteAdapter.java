package br.com.viattec.imobapp.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.viattec.imobapp.R;
import br.com.viattec.imobapp.model.Cliente;

/**
 * Created by Jaciel on 18/09/2015.
 */
public class ClienteAdapter extends RecyclerView.Adapter<ClienteAdapter.ClientesViewHolder> {

    private Context mContext;
    private List<Cliente> mClientes;
    private AoClicarNoClienteListener mListener;

    public ClienteAdapter(Context ctx, List<Cliente> clientes){
        mContext = ctx;
        mClientes = clientes;
    }

    public void setAoClicarNoClienteListener(AoClicarNoClienteListener l){
        mListener = l;
    }

    @Override
    public ClientesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.card_cliente, parent, false);
        ClientesViewHolder vh = new ClientesViewHolder(v);
        v.setTag(vh);
        v.setOnClickListener(new View.OnClickListener() {
                                 @Override
                                 public void onClick(View v) {
                                     if (mListener != null) {
                                         ClientesViewHolder vh = (ClientesViewHolder) v.getTag();
                                         int position = vh.getPosition();
                                         Cliente cliente = mClientes.get(position);
                                         mListener.aoClicarNoCliente(v, position, mClientes.get(position),cliente.getId());
                                     }
                                 }
                             }
        );
        return vh;
    }

    @Override
    public void onBindViewHolder(ClienteAdapter.ClientesViewHolder holder, int position) {
        Cliente cliente = mClientes.get(position);
        holder.txtNome.setText(cliente.getNome());
        holder.txtCpf.setText("CPF: "+cliente.getCpf());
        holder.txtRg.setText("RG: "+cliente.getRg());
        holder.txtTel1.setText("Tel1: "+cliente.getTelefone1());
        holder.txtTel2.setText("Tel2: "+cliente.getTelefone2());
        holder.txtTel3.setText("Tel3: "+cliente.getTelefone3());
    }

    @Override
    public int getItemCount() {
        return mClientes != null ? mClientes.size():0;
    }

    public interface AoClicarNoClienteListener{
        void aoClicarNoCliente(View v, int position, Cliente cliente, String idCliente);
    }

    public static class ClientesViewHolder extends RecyclerView.ViewHolder{

        public TextView txtNome;
        public TextView txtTel1;
        public TextView txtTel2;
        public TextView txtTel3;
        public TextView txtCpf;
        public TextView txtRg;

        public ClientesViewHolder(View parent) {
            super(parent);
            txtNome = (TextView) parent.findViewById(R.id.txtNome);
            txtTel1 = (TextView) parent.findViewById(R.id.txtTel1);
            txtTel2 = (TextView) parent.findViewById(R.id.txtTel2);
            txtTel3 = (TextView) parent.findViewById(R.id.txtTel3);
            txtCpf = (TextView) parent.findViewById(R.id.txtCpf);
            txtRg = (TextView) parent.findViewById(R.id.txtRg);
        }
    }
}
