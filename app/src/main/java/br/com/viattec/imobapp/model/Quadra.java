package br.com.viattec.imobapp.model;

import java.util.List;

/**
 * Created by Jaciel on 14/09/2015.
 */
public class Quadra {

    private String descricao;
    private Loteamento loteamento;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
