package br.com.viattec.imobapp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Jaciel on 02/11/2015.
 */
public class ImobAppUtil {

    public static boolean isCheckNetwork(Context context){
        boolean networkValidate = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager == null){
            return networkValidate;
        }
        NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
        if(info != null){
            for (int i = 0; i < info.length; i++) {
                if(info[i].getState() == NetworkInfo.State.CONNECTED){
                    System.out.println(info[i]);
                    networkValidate = true;
                }
            }
        }
        return networkValidate;
    }
}
